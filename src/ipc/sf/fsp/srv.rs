use crate::ipc::sf;
use crate::mem;
use crate::result::*;
use crate::version;

ipc_sf_define_interface_trait! {
    trait IFileSystemProxy {
        set_current_process [1, version::VersionInterval::all()]: (process_id: sf::ProcessId) => ();
        open_filesystem_with_id_pre_16_0_0 [8, version::VersionInterval::from_to(version::Version::new(3, 0, 0), version::Version::new(15, 0, 1))]: (path_buf: sf::InFixedPointerBuffer<super::Path>, proxy_type: super::FileSystemProxyType, title_id: u64) => (filesystem: mem::Shared<dyn super::IFileSystem>);
        open_filesystem_with_id_post_16_0_0 [10, version::VersionInterval::from(version::Version::new(16, 0, 0))]: (path_buf: sf::InFixedPointerBuffer<super::Path>, attr: super::ContentAttributes, proxy_type: super::FileSystemProxyType, title_id: u64) => (filesystem: mem::Shared<dyn super::IFileSystem>);
        open_bis_filesystem [11, version::VersionInterval::all()]: (string: sf::InFixedPointerBuffer<super::Path>, partition: sf::EnumAsPrimitiveType<super::BisPartitionId, u32>) => (filesystem: mem::Shared<dyn super::IFileSystem>);
        open_bis_storage [12, version::VersionInterval::all()]: (partition: sf::EnumAsPrimitiveType<super::BisPartitionId, u32>) => (filesystem: mem::Shared<dyn super::IStorage>);
        open_sd_card_filesystem [18, version::VersionInterval::all()]: () => (sd_filesystem: mem::Shared<dyn super::IFileSystem>);
        get_rights_id_and_key_generation_by_path_pre_16_0_0 [610, version::VersionInterval::from_to(version::Version::new(3, 0, 0), version::Version::new(15, 0, 1))]: (path_buf: sf::InFixedPointerBuffer<super::Path>) => (key_generation: u64, rights_id: super::RightsId);
        get_rights_id_and_key_generation_by_path_post_16_0_0 [610, version::VersionInterval::from(version::Version::new(16, 0, 0))]: (path_buf: sf::InFixedPointerBuffer<super::Path>, attr: sf::EnumAsPrimitiveType<super::ContentAttributes, u64>) => (key_generation: u64, rights_id: super::RightsId);
        output_access_log_to_sd_card [1006, version::VersionInterval::all()]: (log_buf: sf::InMapAliasBuffer<u8>) => ();
    }
}
