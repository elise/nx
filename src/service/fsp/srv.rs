use crate::ipc::sf;
use crate::ipc::sf::sm;
use crate::mem;
use crate::result::*;
use crate::service;

pub use crate::ipc::sf::fsp::srv::*;

ipc_client_define_object_default!(FileSystemProxy);

impl IFileSystemProxy for FileSystemProxy {
    fn set_current_process(&mut self, process_id: sf::ProcessId) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 1] (process_id) => ())
    }

    fn open_sd_card_filesystem(&mut self) -> Result<mem::Shared<dyn super::IFileSystem>> {
        ipc_client_send_request_command!([self.session.object_info; 18] () => (sd_filesystem: mem::Shared<super::FileSystem>))
    }

    fn output_access_log_to_sd_card(&mut self, access_log: sf::InMapAliasBuffer<u8>) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 1006] (access_log) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn open_bis_filesystem(
        &mut self,
        string: sf::InFixedPointerBuffer<super::Path>,
        partition: sf::EnumAsPrimitiveType<super::BisPartitionId, u32>,
    ) -> Result<(mem::Shared<dyn super::IFileSystem>)> {
        ipc_client_send_request_command!([self.session.object_info; 11] (string, partition) => (filesystem: mem::Shared<super::FileSystem>))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn open_filesystem_with_id_pre_16_0_0(
        &mut self,
        path_buf: sf::InFixedPointerBuffer<super::Path>,
        proxy_type: super::FileSystemProxyType,
        title_id: u64,
    ) -> Result<(mem::Shared<dyn super::IFileSystem>)> {
        ipc_client_send_request_command!([self.session.object_info; 8] (path_buf, proxy_type, title_id) => (filesystem: mem::Shared<super::FileSystem>))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn open_filesystem_with_id_post_16_0_0(
        &mut self,
        path_buf: sf::InFixedPointerBuffer<super::Path>,
        attr: super::ContentAttributes,
        proxy_type: super::FileSystemProxyType,
        title_id: u64,
    ) -> Result<(mem::Shared<dyn super::IFileSystem>)> {
        ipc_client_send_request_command!([self.session.object_info; 10] (path_buf, attr, proxy_type, title_id) => (filesystem: mem::Shared<super::FileSystem>))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_rights_id_and_key_generation_by_path_pre_16_0_0(
        &mut self,
        path_buf: sf::InFixedPointerBuffer<super::Path>,
    ) -> Result<(u64, super::RightsId)> {
        ipc_client_send_request_command!([self.session.object_info; 610] (path_buf) => (key_generation: u64, rights_id: super::RightsId))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_rights_id_and_key_generation_by_path_post_16_0_0(
        &mut self,
        path_buf: sf::InFixedPointerBuffer<super::Path>,
        attr: sf::EnumAsPrimitiveType<super::ContentAttributes, u64>,
    ) -> Result<(u64, super::RightsId)> {
        ipc_client_send_request_command!([self.session.object_info; 610] (path_buf, attr) => (key_generation: u64, rights_id: super::RightsId))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn open_bis_storage(
        &mut self,
        partition: sf::EnumAsPrimitiveType<super::BisPartitionId, u32>,
    ) -> Result<(mem::Shared<dyn super::IStorage>)> {
        ipc_client_send_request_command!([self.session.object_info; 12] (partition) => (storage: mem::Shared<super::Storage>))
    }
}

impl service::IService for FileSystemProxy {
    fn get_name() -> sm::ServiceName {
        sm::ServiceName::new("fsp-srv")
    }

    fn as_domain() -> bool {
        true
    }

    fn post_initialize(&mut self) -> Result<()> {
        self.set_current_process(sf::ProcessId::new())
    }
}
